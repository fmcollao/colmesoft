<?php declare(strict_types=1);
session_start();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

if (PHP_SAPI == 'cli-server') {
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require_once __DIR__ . '/../vendor/autoload.php';

$configuraciones = require_once __DIR__ . '/../core/config.php';

$app = new \Slim\App($configuraciones);

require_once __DIR__ . '/../core/dependencias.php';
require_once __DIR__ . '/../core/middleware.php';
require_once __DIR__ . '/../core/routes.php';