<?php declare(strict_types=1);

return [
	'settings' => [
		'displayErrorDetails' => TRUE,
		'addContentLengthHeader' => FALSE,
		'db' => [
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'colmesoft',
			'username'  => 'root',
			'password'  => 'adminbd',
			'charset'   => 'utf8mb4',
			'collation' => 'utf8mb4_unicode_ci',
			'prefix'    => ''
		]
	]
];