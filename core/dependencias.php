<?php declare(strict_types=1);

/*
 * CONTENEDOR PRINCIPAL
 */
$contenedor = $app->getContainer();

/*
 * CONFIGURACION ELOQUENT ORM
 */
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($contenedor['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$contenedor['db'] = function ($contenedor) {
	return $capsule;
};

/*
 * CONFIGURACION VISTAS TWIG
 */
$contenedor['view'] = function ($contenedor) {
	$view = new \Slim\Views\Twig(__DIR__ . '/../resources/views/', [
		'cache' => FALSE
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$contenedor->router,
		$contenedor->request->getUri())
	);

	return $view;
};

/*
 * CONTROLLERS DEL SITIO
 */
$contenedor['HomeController'] = function ($contenedor) {
	return new \App\Controllers\HomeController($contenedor);
};

$contenedor['AuthController'] = function ($contenedor) {
	return new \App\Controllers\AuthController($contenedor);
};