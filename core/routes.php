<?php declare(strict_types=1);

$app->get('/', 'HomeController:index')->setName('home');
$app->get('/auth/registro', 'AuthController:getRegistro')->setName('auth.registro');
$app->post('/auth/registro', 'AuthController:postRegistro');
$app->get('/auth/login', 'AuthController:getLogin')->setName('auth.login');
$app->post('/auth/login', 'AuthController:postLogin');
