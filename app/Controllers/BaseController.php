<?php declare(strict_types=1);
namespace App\Controllers;

class BaseController
{
	protected $contenedor;

	public function __construct($contenedor)
	{
		$this->contenedor = $contenedor;
	}

	public function __get($propiedad)
	{
		if ($this->contenedor->{$propiedad}) {
			return $this->contenedor->{$propiedad};
		}
	}
}