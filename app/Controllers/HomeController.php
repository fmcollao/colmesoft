<?php declare(strict_types=1);
namespace App\Controllers;

class HomeController extends BaseController
{
	public function index($request, $response)
	{
		return $this->view->render($response, 'home/home.html.twig');
	}
}