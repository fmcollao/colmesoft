<?php declare(strict_types=1);
namespace App\Controllers;

use App\Models\User;

class AuthController extends BaseController
{
	public function getRegistro($request, $response)
	{
		return $this->view->render($response, 'auth/registro.html.twig');
	}

	public function postRegistro($request, $response)
	{
		$nombre = filter_var(trim($request->getParam('nombre')), FILTER_SANITIZE_STRING);
		$apellido = filter_var(trim($request->getParam('apellido')), FILTER_SANITIZE_STRING);
		$email = filter_var(trim($request->getParam('email')), FILTER_SANITIZE_EMAIL);
		$password = filter_var(trim($request->getParam('password')), FILTER_SANITIZE_EMAIL);

		$usuario = User::create([
			'nombre' => $nombre,
			'apellido' => $apellido,
			'email' => $email,
			'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]),
		]);

		return $response->withRedirect($this->router->pathFor('home'));
	}

	public function getLogin($request, $response)
	{
		return $this->view->render($response, 'auth/login.html.twig');
	}

	public function postLogin()
	{
		//
	}
}