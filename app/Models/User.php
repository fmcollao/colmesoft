<?php declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class User extends Model
{
	protected $table = 'users';
	protected $fillable = [
		'nombre',
		'apellido',
		'email',
		'password',
	];
}